import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.applet.Applet;

public class CarApplet extends Applet implements ActionListener{
	Button btn1 = new Button("Start");
	Button btn2 = new Button("Add");
	Label lab1, lab2;
	TextField t1;
	CarSimulation sim;
	Boolean start_flag = false;

	public void init(){
		setBackground(Color.ORANGE);
		lab1 = new Label("Press Start");
		lab2 = new Label("Round 0");
		t1 = new TextField();
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		add(lab1);
		add(lab2);
		add(t1);
		add(btn1);
		add(btn2);
	}

	public void actionPerformed(ActionEvent e){
		if( start_flag == false ){
			int temp = Integer.parseInt(t1.getText());
			sim = new CarSimulation(temp);
			lab1.setLocation(350,150);
			lab2.setLocation(350,100);
			btn1.setLocation(350,300);
			btn2.setLocation(400,300);
			sim.print_highway(lab1,lab2);
			start_flag = true;
		}
		else{
			lab1.setLocation(350,150);
			lab2.setLocation(350,100);
			btn1.setLocation(350,300);
			btn2.setLocation(400,300);
			if( e.getSource() == btn1 )
				sim.simulation(lab1,lab2);
			else if( e.getSource() == btn2 ){
				sim.insert();
				sim.print_highway(lab1,lab2);
			}
		}
	}
}
