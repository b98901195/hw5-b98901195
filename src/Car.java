public class Car extends Thread{
	//Local Variables//
	private int speed;
	private int target_speed;
	private int state;
	private int decision;
	private int round;
	private Boolean finish_flag;
	private CarSimulation sim;

	//Constructor//
	Car(int speed, int round, CarSimulation sim){
		this.speed = speed;
		this.target_speed = -1;
		this.state = 0;
		this.decision = -1;
		this.round = round;
		this.finish_flag = false;
		this.sim = sim;
	}

	//run function//
	public void run(){
		while(sim.continue_flag && !finish_flag){
			if( round < sim.counter ){
				decide_and_move();
				round++;
			}
			else{
				sleeptowait(100);
			}
		}
		
	}

	//make decision and move//
	private void decide_and_move(){
		make_decision();
		sim.move_car(this, speed);
		update_speed();
	}

	//make decision//
	private void make_decision(){
		int distance = sim.get_front_position(this) - sim.get_position(this);
		int safe_speed = calculate_speed(distance);
		if( decision <= 0 ){
			if( speed > safe_speed )			//slows down//
				decision = 1;
			else if( speed < safe_speed )			//speeds up//
				decision = 2;
			else if( speed == safe_speed )			//remains the same//
				decision = 0;
			else						//error occurs//
				System.out.println("Something went wrong");
			state = 0;
			target_speed = safe_speed;
		}
	}

	//calculate speed//
	private int calculate_speed(int distance){
		if( distance/2 > 4 )
			return 4;
		else
			return (int)Math.floor(distance/2);
	}
	
	//update speed//
	private void update_speed(){
		if( decision == 0 ){					//remains the same//
			decision = -1;
			target_speed = -1;
			state = 0;
		}
		else if( decision == 1 ){				//slows down//
			decision = -1;
			speed = target_speed;
			target_speed = -1;
			state = 0;
		}
		else if( decision == 2 && state == 0 ){			//speeds up//
			state = 1;
		}
		else if( decision == 2 && state == 1 ){			//speeds up//
			speed = target_speed;
			target_speed = -1;
			state = 2;
		}
		else if( decision == 2 && state == 2 ){			//speeds up//
			decision = -1;
			state = 0;
		}
		else{
			System.out.println("Something went wrong");
		}
	}

	//let others to modify the flag to make the thread stop//
	public void finish_road(){
		finish_flag = true;
	}

	//sleep function//
	private void sleeptowait(int time){
		try{
			sleep(time);
		}
		catch(InterruptedException e){
			System.out.println("Something went wrong");
		}
	}
}
