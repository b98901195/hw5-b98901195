import java.awt.*;
import java.awt.event.*;

public class CarSimulation{
	//Local Variables//
	private int highway_len;
	private Car[] car_array;
	public int counter;
	public Boolean continue_flag;
	public Boolean accident_flag;

	//Constructors//
	CarSimulation(int highway_len){
		this.highway_len = highway_len;
		this.car_array = new Car[highway_len];
		for( int i = 0; i < highway_len; i++ )
			car_array[i] = null;
		this.counter = 0;
		this.continue_flag = true;
		this.accident_flag = false;
	}
	CarSimulation(){
		this(100);
	}

	//Simulation//
	public void simulation(Label lb1, Label lb2){
			print_highway(lb1, lb2);
			//insert();
			//print_highway(lb);
			decision_and_move();
			check_accident();
			sleeptowait(500);
			print_highway(lb1, lb2);
	}

	//Print Function//
	public void print_highway(Label lb1, Label lb2){
		String s = null;
		StringBuffer buf = new StringBuffer();
		for( int i = 0; i < highway_len; i++ )
				if( car_array[i] != null )
					buf.append("x");
				else
					buf.append(".");
		s = buf.toString();
		lb1.setText(s);
		lb2.setText("Round "+counter);
	}

	//Insert Car//
	public void insert(){
		int distance = find_first_car();
		if( distance >= 2 ){
			car_array[0] = new Car(calculate_speed(distance), counter, this);
			car_array[0].start();
		}
	}

	//Find the first car//
	private int find_first_car(){
		for( int i = 0; i < highway_len; i++ )
			if( car_array[i] != null )
				return i;
		return highway_len;
	}

	//Calculate safe speed//
	private int calculate_speed(int distance){
		if( distance/2 > 4 )
			return 4;
		else
			return (int)Math.floor(distance/2);
	}

	//Allow the cars to make decision and move//
	private void decision_and_move(){
		counter++;
	}
	
	//Check accidents//
	private void check_accident(){
		if( accident_flag == true ){
			System.out.println("Accident Occurs");
			continue_flag = false;
		}
	}

	//Sleep function//
	private void sleeptowait(int time){
		try{
			Thread.sleep(time);
		}
		catch(InterruptedException e){
			System.out.println("Something went wrong");
		}
	}

	//return car position//
	public int get_position(Car car){
		for( int i = 0; i < highway_len; i++ )
			if( car_array[i] == car )
				return i;
		return -1;
	}

	//return the front car position//
	public int get_front_position(Car car){
		int flag = 0;
		for( int i = 0; i < highway_len; i++ ){
			if( flag == 1 && car_array[i] != null )
				return i;
			if( car_array[i] == car )
				flag = 1;
		}
		return highway_len*2; 
	}

	//function for cars to request a move//
	public void move_car(Car car, int speed){
		int pos = get_position(car);
		//System.out.printf("%d %d\n", pos, speed);
		if( pos != -1 ){
			if( highway_len > speed + pos ){
				if( car_array[speed+pos] != null ){
					car_array[speed+pos].finish_road();
					accident_flag = true;
				}
				car_array[speed+pos] = car_array[pos];
				car_array[pos] = null;
			}
			else
				car_array[pos].finish_road();
				car_array[pos] = null;
		}
	}
}
