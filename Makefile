default:
	javac -d ./ ./src/Car.java ./src/CarSimulation.java ./src/CarApplet.java
	cp ./src/MANIFEST.MF ./MANIFEST.MF
	jar cvfm CarApplet.jar ./MANIFEST.MF ./Car.class ./CarSimulation.class ./CarApplet.class
	rm -f ./Car.class ./CarSimulation.class ./CarApplet.class ./MANIFEST.MF

clean:
	rm -f ./CarApplet.jar
